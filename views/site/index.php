<?php

/* @var $this yii\web\View */
use app\models\Quiz;
use yii\helpers\Url;

$quiz = Quiz::find()->all();

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bem vindo ao Quiz!</h1>

        <p class="lead"></p>
    </div>

    <div class="body-content">
        
        <?php foreach($quiz as $reg){ ?>
        <div class="row">
            <div class="col-lg-12">
                <h2><?=$reg->name?></h2>

                <p><?=$reg->description?></p>

                <p><a class="btn btn-default" href="<?= Url::to(['quiz/index','id'=>$reg->quiz_id]) ?>">Iniciar quiz &raquo;</a></p>
            </div>
        </div>
        <hr />
        <?php } ?>
    </div>
</div>
