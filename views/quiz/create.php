<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Quiz;
use yii\bootstrap\ActiveForm;
use app\models\Answer;
use app\models\Questions;

$this->title = 'Responder';
$this->params['breadcrumbs'][] = ['label' => 'Iniciar Quiz', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$quest = Questions::findAll(['quiz_id' => $id]);
?>
<div class="site-about">
    <?php 
    $frm = ActiveForm::begin(['id' => 'contact-form', 'action' => 'post']);
    $qe = '';
    $i = 1;
    echo $frm->field($model, 'start_date')->hiddenInput()->label(false);
    echo $frm->field($model, 'email')->hiddenInput()->label(false);
    echo $frm->field($model, 'quiz_id')->hiddenInput(['value' => $id])->label(false);
    foreach($quest as $reg){
        echo "<h3>Pergunta - ".$i."</h3>";
        echo "<p>".$reg->subject."</p>";
        $model1 = Answer::findAll(['question_id' => $reg->quest_id]);
        foreach($model1 as $row){
            $m = Questions::findOne($row->question_id);
            $qe = $frm->field($m, 'quest_id')->hiddenInput()->label(false);
    ?>    
        <div class="form-group" style="display:table">
            <div class="col-lg-12 text-left">
            <input type="checkbox" id="answer-answers_id" name="Answer[answers_id][]" value="<?=$row->answers_id?>"> <?= $row->answer?> 
            </div>
        </div>    
    <?php
        }
        $i++;
    }
    echo $qe;
    ?>
    <div>
    <br>
    <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
