<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Quiz;
use yii\bootstrap\ActiveForm;

$this->title = 'Iniciar Quiz';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="site-about">
    <h1 class="text-center"><?=$model->name?></h1>
    <p class="text-center">
        <?=$model->description?>
    </p>
    <br /><br />
    <div class="col-md-4 text-center" style="margin: 0 auto; float:none">
        <p>
            Digite seu e-mail para começar.
        </p>
        
        <?php $frm = ActiveForm::begin(['id' => 'contact-form', 'action' => 'responder?id='.$id]); ?>
            <?= $frm->field($form, 'email')->textInput()->label(false) ?>
            <div class="form-group">
                <?= Html::submitButton('Iniciar', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
