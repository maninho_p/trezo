<?php

namespace app\modules\admin;
use Yii;
use app\modules\admin\models\LoginForm;
/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        Yii::$app->name = "Administrador";

        if(Yii::$app->user->isGuest){
            return $this->layout = '@app/modules/admin/views/layouts/login';
        }
        
        // custom initialization code goes here
    }
    
}
