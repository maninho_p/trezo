<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use Yii;
use app\modules\admin\models\LoginForm;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect('admin/quiz');
    }

    public function actionLogin($url){
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect($url);
        }else{
            Yii::$app->getSession()->setFlash('error', 'Usuário e senha inválido.');
        }

        return $this->redirect($url);
        //return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
    }
}
