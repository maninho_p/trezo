<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Questions;
use app\models\QuestionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Answer;

/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($quiz,$id=false)
    {
        if(!$id)
            $model = new Questions();
        else
            $model = Questions::findOne($id);    
        $model->created_at = new \yii\db\Expression('NOW()');
        $model->updated_at = new \yii\db\Expression('NOW()');
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if(count(Yii::$app->request->post('Answer')['answer']) > 0){
                foreach(Yii::$app->request->post('Answer')['answer'] as $l => $reg){
                    $respostas = new Answer();
                    if(Yii::$app->request->post('Answer')['answers_id'][$l]){
                        $respostas = Answer::findOne(Yii::$app->request->post('Answer')['answers_id'][$l]);
                        $respostas->updated_at = new \yii\db\Expression('NOW()');  
                    }else{
                        $respostas->created_at = new \yii\db\Expression('NOW()');
                        $respostas->updated_at = new \yii\db\Expression('NOW()');  
                        $respostas->question_id = $model->quest_id;
                    }

                    $respostas->answer = $reg;

                    if(Yii::$app->request->post('Answer')['is_correct'][$l])
                        $respostas->is_correct = 1;
                    else
                        $respostas->is_correct = 0;
                    
                    $respostas->save();
                }
            }
            return $this->redirect(['quiz/view', 'id' => $model->quiz_id]);
        }

        if(Yii::$app->getRequest()->isAjax){
            return $this->renderAjax('create', [
                'model' => $model,
                'quiz' => $quiz
            ]);
        }else{
            return $this->render('create', [
                'model' => $model,
                'quiz' => $quiz
            ]);
        }
    }

    /**
     * Updates an existing Questions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['quiz/view', 'id' => $model->quiz_id]);
        }

        if(Yii::$app->getRequest()->isAjax){
            return $this->renderAjax('create', [
                'model' => $model,
                'quiz' => $model->quiz_id
            ]);
        }else{
            return $this->render('create', [
                'model' => $model,
                'quiz' => $model->quiz_id
            ]);
        }
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeletar($id)
    {
        $model = $this->findModel($id);
        $id = $model->quiz_id;
        $model->delete();

        return $this->redirect(['quiz/view', 'id' => $id]);
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
