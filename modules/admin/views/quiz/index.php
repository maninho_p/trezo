<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use prawee\widgets\ButtonAjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QuizSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quiz';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
function AbrirModal(url){
    $("#main-modal").modal("show")
    .find("#main-content-modal")
    .load(url);
}

$("#main-modal").on("hidden.bs.modal", function () {
    $("#main-modal .modal-body").html("<div id=\'main-content-modal\'><p class=\'text-center\'>Carregando...</p></div>");
});
');
?>
<div class="quiz-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
    <?= ButtonAjax::widget([
        'name'=>'Novo',
        'route'=>['create'],
        'modalId'=>'#main-modal',
        'modalContent'=>'#main-content-modal',
        'options'=>[
            'class'=>'btn btn-success',
            'title'=>'Button for create application',
        ]
    ]);?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return Yii::$app->formatter->asDateTime($model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model){
                    return Yii::$app->formatter->asDateTime($model->updated_at);
                }
            ],
            [
                'label' => 'Perguntas',
                'value' => function($model){
                    return app\models\Questions::find()->where(['quiz_id' => $model->quiz_id])->count();
                }
            ],
            [
                'label' => 'Respondido',
                'format' => 'html',
                'value' => function($model){
                    $quiz = app\models\Questions::find()
                    ->where(['quiz_id' => $model->quiz_id])
                    ->all();
                    $ids = array();
                    foreach($quiz as $row){
                        $ids[] = $row->quest_id;
                    }

                    return "<a href='".Url::to(['quiz-user/index','QuizuserSearch[quiz_id]' => $model->quiz_id])."'>".app\models\QuizUser::find()->where(['quest_id' => $ids])->count()."</a>";
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn','template' => '{view} {update} {delete}',
                'buttons' => [
                    'update' => function ($url,$model) {
                        $this->registerJs('
                        $("span.glyphicon.glyphicon-pencil").click(function(e){
                            e.preventDefault();
                            AbrirModal("'.$url.'");
                        });
                        '
                        );

                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>', 
                            $url);
                    }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
