<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Quiz */

$this->title = 'Cadastrar Quiz';
$this->params['breadcrumbs'][] = ['label' => 'Quiz', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quiz-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
