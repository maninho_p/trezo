<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use prawee\widgets\ButtonAjax;

/* @var $this yii\web\View */
/* @var $model app\models\Quiz */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Quiz', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quiz-view">

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'description',
            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return Yii::$app->formatter->asDateTime($model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model){
                    return Yii::$app->formatter->asDateTime($model->updated_at);
                }
            ]
        ],
    ]);
    ?>
    <?= ButtonAjax::widget([
        'name'=>'Editar',
        'route'=>['update','id' => $model->quiz_id],
        'modalId'=>'#main-modal',
        'modalContent'=>'#main-content-modal',
        'options'=>[
            'class'=>'btn btn-primary',
            'title'=>'Button for create application',
        ]
    ]);?>
    <hr />
    <?php 
        echo $this->render('../questions/index', [
            'searchModel' => $searchModelQuestion,
            'dataProvider' => $dataProviderQuestion,
            'quiz' => $model->quiz_id
        ]);
    ?>

</div>
