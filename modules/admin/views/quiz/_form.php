<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Quiz */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="quiz-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nome') ?>

    <?= $form->field($model, 'description')->textarea(['rows' => '6'])->label('Descrição') ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
        <?php
        if($model->quiz_id){
            echo Html::a('Deletar', ['delete', 'id' => $model->quiz_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Deseja remover esse quiz?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
