<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\modules\admin\models\LoginForm;
use yii\helpers\Url;


$this->title = 'Login';

$model = new LoginForm();
?>
<div class="site-login text-center">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Por favor, entre com o login e senha para acessar:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'action' => Url::to(['default/login','url'=>Yii::$app->getRequest()->getUrl()]),
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Login') ?>

        <?= $form->field($model, 'password')->passwordInput()->label('Senha') ?>

        <div class="form-group">
            <div class="col-lg-12">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary form-control', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
