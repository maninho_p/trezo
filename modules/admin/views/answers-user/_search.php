<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AnswersUserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="answers-user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'user_answer_id') ?>

    <?= $form->field($model, 'quiz_user_id') ?>

    <?= $form->field($model, 'answers_id') ?>

    <?= $form->field($model, 'answers_user') ?>

    <?= $form->field($model, 'acerto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
