<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AnswersUser */

$this->title = 'Update Answers User: ' . $model->user_answer_id;
$this->params['breadcrumbs'][] = ['label' => 'Answers Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_answer_id, 'url' => ['view', 'id' => $model->user_answer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="answers-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
