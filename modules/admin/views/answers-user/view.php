<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AnswersUser */

$this->title = $model->user_answer_id;
$this->params['breadcrumbs'][] = ['label' => 'Answers Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answers-user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->user_answer_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->user_answer_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_answer_id',
            'quiz_user_id',
            'answers_id',
            'answers_user',
            'acerto',
        ],
    ]) ?>

</div>
