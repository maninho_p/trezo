<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AnswersUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="answers-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'answers_id')->textInput() ?>

    <?= $form->field($model, 'answers_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'acerto')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
