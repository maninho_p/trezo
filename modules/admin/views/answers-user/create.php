<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AnswersUser */

$this->title = 'Create Answers User';
$this->params['breadcrumbs'][] = ['label' => 'Answers Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answers-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
