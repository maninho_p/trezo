<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use prawee\widgets\ButtonAjax;
use app\models\Answer;
/* @var $this yii\web\View */
/* @var $searchModel app\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Questões';

$this->registerJs('
function AbrirModal(url){
    $("#main-modal").modal("show")
    .find("#main-content-modal")
    .load(url);
}

$("#main-modal").on("hidden.bs.modal", function () {
    $("#main-modal .modal-body").html("<div id=\'main-content-modal\'><p class=\'text-center\'>Carregando...</p></div>");
});

$(".DeleteQuest").on("click",function(e){
    var r = confirm("Deseja remover essa questão?");
    if (r == false) {
        e.preventDefault();
    }
});

');
?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?= ButtonAjax::widget([
        'name'=>'Adicionar',
        'route'=>['questions/create','quiz' => $quiz],
        'modalId'=>'#main-modal',
        'modalContent'=>'#main-content-modal',
        'options'=>[
            'class'=>'btn btn-success',
            'title'=>'Button for create application',
        ]
    ]); ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'subject:ntext',
            [
                'attribute' => 'type',
                'value' => function($model){
                    return $model->type? 'Sim' : 'Não';
                }
            ],
            [
                'label' => 'Acertos',
                'value' => function($model){

                    $quest = app\models\QuizUser::find()->where(['quest_id' => $model->quest_id])->all();
                    $ids = array();
                    foreach($quest as $row){
                        $ids[] = $row->quiz_user_id;
                    }

                    return app\models\AnswersUser::find()->where(['quiz_user_id' => $ids, 'acerto' => 1])->count();
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return Yii::$app->formatter->asDateTime($model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model){
                    return Yii::$app->formatter->asDateTime($model->updated_at);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url,$model) {
                        $this->registerJs('
                        $("span.glyphicon.glyphicon-pencil").click(function(e){
                            e.preventDefault();
                            AbrirModal("'.Url::to(['questions/create','id' => $model->quest_id]).'&quiz='.$model->quiz_id.'");
                        });
                        '
                        );

                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>', 
                            Url::to(['questions/create','id' => $model->quest_id])."&quiz=".$model->quiz_id);
                    },
                    'delete' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash DeleteQuest"></span>',
                            Url::to(['questions/deletar','id' => $model->quest_id])
                            );
                    }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
