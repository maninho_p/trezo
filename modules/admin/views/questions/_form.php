<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Answer;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */
/* @var $form yii\widgets\ActiveForm */

$js = 'var form1 = "";';

if($model->quest_id){
    $m = Answer::findAll(['question_id' => $model->quest_id]);
    $i = 1;
    foreach($m as $row){
        if($i == 1){
            $js .= '
            $(".tabela").show().removeClass("hide");
            ';
        }
        $js .= '
        form1 += "<table id=\'remove'.$i.'\' width=\'100%\' style=\'margin-bottom:10px\' class=\'tabela\'><tr><td style=\'width:10%\'><a href=\'javascript:void(0)\' class=\'remove\' data-value=\'remove'.$i.'\' data-reg=\''.$row->answers_id.'\'>Remover</a></td><td style=\'width:70%; padding-right: 10px\'><input type=\'text\' class=\'form-control\' name=\'Answer[answer]['.$i.']\' value=\''.$row->answer.'\' maxlength=\'255\' aria-invalid=\'false\'></td><td> <input type=\'hidden\' name=\'Answer[answers_id]['.$i.']\' value=\''.$row->answers_id.'\' /><input type=\'hidden\' name=\'Answer[is_correct]['.$i.']\' value=\'0\' /><input type=\'checkbox\' name=\'Answer[is_correct]['.$i.']\' value=\'1\' '.($row->is_correct? 'checked':'').' /></td></tr></table>";
        ';
        $i++;
    }
    
    $js .= '
    $(".opcoes").append(form1);
    $(".remove").on("click",function(){
        var op = $(this);
        var r = confirm("Deseja remover essa opção?");
        if (r == true) {
            $.post("'.Url::to(['answer/delete']).'?id="+$(this).attr("data-reg"),function(){
                RmOpcao(op);
            });
        }         
    });
    ';
}

$js .= '
    var add = function(){
        var i = $(".tabela").length;
        var form = "<table id=\'remove"+i+"\' width=\'100%\' style=\'margin-bottom:10px\' class=\'tabela\'><tr><td style=\'width:10%\'><a href=\'javascript:void(0)\' class=\'remove\' data-value=\'remove"+i+"\' data-reg=\'0\'>Remover</a></td><td style=\'width:70%; padding-right: 10px\'><input type=\'text\' class=\'form-control\' name=\'Answer[answer]["+i+"]\' value=\'\' maxlength=\'255\' aria-invalid=\'false\'></td><td> <input type=\'hidden\' name=\'Answer[answers_id]["+i+"]\' value=\'0\' /><input type=\'hidden\' name=\'Answer[is_correct]["+i+"]\' value=\'0\' /><input type=\'checkbox\' name=\'Answer[is_correct]["+i+"]\' value=\'1\' /></td></tr></table>";

        $(".tabela").show().removeClass("hide");
        
        $(".opcoes").append(form);
        $(".remove").on("click",function(){
            RmOpcao($(this));
        });
    };

    $(".adicionar").on("click",add);

    function RmOpcao(op){
        var id = op.attr("data-value");
        $("#"+id).remove();
        if($(".tabela").length <= 1){
            $(".tabela").addClass("hide");
        }
    }    
';


$this->registerJs($js);
?>

<div class="questions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'quiz_id')->hiddenInput(['value' => $quiz])->label(false) ?>

    <?= $form->field($model, 'subject')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'type')->checkbox() ?>

    <hr />
    <h2>Opções</h2>
    <div class="form-group">
        <?= Html::button('Adicionar +', ['class' => 'btn btn-primary adicionar']) ?>
    </div>

    <div class="form-group opcoes">
        <table width='100%' class="tabela hide">
            <tr>
                <td style='width: 10%'>Ação</td>
                <td style='width: 70%'>Resposta</td>
                <td style='width: 20%'>Resp. Correta</td>
            </tr>

        </table>    
    </div>

    <div class="form-group text-right">
        <?php
        if($model->quest_id){
            echo Html::a('Deletar', ['deletar', 'id' => $model->quest_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Deseja remover essa questão?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
