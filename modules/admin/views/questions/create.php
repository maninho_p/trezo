<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = 'Cadastrar Questão';
$this->params['breadcrumbs'][] = ['label' => 'Questões', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'quiz' => $quiz
    ]) ?>

</div>
