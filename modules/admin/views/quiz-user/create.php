<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QuizUser */

$this->title = 'Create Quiz User';
$this->params['breadcrumbs'][] = ['label' => 'Quiz Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quiz-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
