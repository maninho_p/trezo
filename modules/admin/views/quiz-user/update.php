<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QuizUser */

$this->title = 'Update Quiz User: ' . $model->quiz_user_id;
$this->params['breadcrumbs'][] = ['label' => 'Quiz Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->quiz_user_id, 'url' => ['view', 'id' => $model->quiz_user_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="quiz-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
