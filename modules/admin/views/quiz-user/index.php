<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\QuizuserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Respondidos';
$this->params['breadcrumbs'][] = ['label' => 'Quiz', 'url' => ['quiz/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quiz-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'email:email',
            'quest_id',
            'quest.subject',
            [
                'attribute' => 'start_date',
                'value' => function($model){
                    return Yii::$app->formatter->asDateTime($model->start_date);
                }
            ],
            [
                'attribute' => 'end_date',
                'value' => function($model){
                    return Yii::$app->formatter->asDateTime($model->end_date);
                }
            ],

            ['class' => 'yii\grid\ActionColumn' ,'template' => '{view} {delete}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
