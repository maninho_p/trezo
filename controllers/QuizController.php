<?php

namespace app\controllers;

use Yii;
use app\models\Quiz;
use app\models\QuizSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\QuizUser;
use yii\helpers\Url;
use app\models\Answer;
use app\models\AnswersUser;
/**
 * QuizController implements the CRUD actions for Quiz model.
 */
class QuizController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Quiz models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $model = Quiz::findOne($id); 
        $form = new QuizUser();
        return $this->render('email', [
            'model' => $model,
            'id' => $id,
            'form' => $form
        ]);
    }
    public function actionPost(){
        if(count(Yii::$app->request->post('QuizUser'))){
            $model = new QuizUser();
            $model->email = Yii::$app->request->post('QuizUser')['email'];    
            $model->quest_id = Yii::$app->request->post('Questions')['quest_id'];    
            $model->start_date = Yii::$app->request->post('QuizUser')['start_date'];  
            $model->quiz_id = Yii::$app->request->post('QuizUser')['quiz_id']; 
            $model->end_date = new \yii\db\Expression('NOW()');
            if($model->save()){
 
                foreach(Yii::$app->request->post('Answer')['answers_id'] as $reg){
                    $model1 = new AnswersUser();
                    $model1->answers_user = $reg;
                    $verifica = Answer::find()->where(['question_id'=>$model->quest_id,'is_correct' => 1])->one();
                    $model1->answers_id = $verifica['answers_id'];
                    $model1->acerto = $verifica['answers_id'] == $reg? 1 : 0;
                    $model1->quiz_user_id = $model->quiz_user_id;
                    $model1->save();
                }
                Yii::$app->getSession()->setFlash('success', 'Sua resposta foi salva com sucesso!');
            }
            
            return $this->redirect(['site/index']);
             
        }
    }

    public function actionResponder($id=0)
    {
        if(Yii::$app->request->post()){
            $c = QuizUser::find()
            ->from(['questions','quiz_user'])
            ->where(['quiz_user.email' => Yii::$app->request->post('QuizUser')['email']])
            ->andWhere('quiz_user.quest_id = questions.quest_id')
            ->andWhere("questions.quiz_id = $id")->all();
            if(count($c) > 0){
                Yii::$app->getSession()->setFlash('error', 'Você ja respondeu esse quiz!');
                return $this->redirect(['site/index']);
            }
            
            $model = new QuizUser();
            $model->email = Yii::$app->request->post('QuizUser')['email'];
            $model->start_date = date('Y-m-d H:i:s');

            return $this->render('create', [
                'model' => $model,
                'id' => $id
            ]);            
        }
      
        return $this->redirect(['site/index']);
    }


    /**
     * Displays a single Quiz model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Quiz model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Quiz();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->quiz_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Quiz model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->quiz_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Quiz model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Quiz model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Quiz the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Quiz::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
