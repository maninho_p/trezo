<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quiz_user".
 *
 * @property int $quiz_user_id
 * @property string $email
 * @property int $quest_id
 * @property string $start_date
 * @property string $end_date
 *
 * @property Questions $quest
 */
class QuizUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quiz_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'quest_id', 'start_date', 'end_date'], 'required'],
            [['quest_id'], 'integer'],
            [['email'], 'email'], 
            [['start_date', 'end_date'], 'safe'],
            [['email'], 'string', 'max' => 255],
            [['quest_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['quest_id' => 'quest_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'quiz_user_id' => 'Id',
            'email' => 'Email',
            'quest_id' => 'Questão',
            'start_date' => 'Inicio',
            'end_date' => 'Fim',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuest()
    {
        return $this->hasOne(Questions::className(), ['quest_id' => 'quest_id']);
    }
}
