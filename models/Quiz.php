<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quiz".
 *
 * @property int $quiz_id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class Quiz extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quiz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'quiz_id' => 'Id',
            'name' => 'Nome',
            'description' => 'Descrição',
            'created_at' => 'Criado em',
            'updated_at' => 'Última atualização',
        ];
    }
}
