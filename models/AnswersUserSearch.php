<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AnswersUser;

/**
 * AnswersUserSearch represents the model behind the search form of `app\models\AnswersUser`.
 */
class AnswersUserSearch extends AnswersUser
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_answer_id', 'quiz_user_id', 'answers_id', 'acerto'], 'integer'],
            [['answers_user'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AnswersUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_answer_id' => $this->user_answer_id,
            'quiz_user_id' => $this->quiz_user_id,
            'answers_id' => $this->answers_id,
            'acerto' => $this->acerto,
        ]);

        $query->andFilterWhere(['like', 'answers_user', $this->answers_user]);

        return $dataProvider;
    }
}
