<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "answers_user".
 *
 * @property int $user_answer_id
 * @property int $answers_id
 * @property string $answers_user
 * @property int $acerto
 *
 * @property Answer $answers
 */
class AnswersUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answers_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['answers_id', 'acerto'], 'required'],
            [['answers_id', 'acerto'], 'integer'],
            [['answers_user'], 'string', 'max' => 11],
            [['answers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answer::className(), 'targetAttribute' => ['answers_id' => 'answers_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_answer_id' => 'User Answer ID',
            'answers_id' => 'Answers ID',
            'answers_user' => 'Answers User',
            'acerto' => 'Acerto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasOne(Answer::className(), ['answers_id' => 'answers_id']);
    }
}
