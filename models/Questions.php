<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property int $quest_id
 * @property int $quiz_id
 * @property string $subject
 * @property int $type
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Answer[] $answers
 * @property Quiz $quiz
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quiz_id', 'subject', 'created_at', 'updated_at'], 'required'], 
            [['quiz_id', 'type'], 'integer'],
            [['subject'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['quiz_id'], 'exist', 'skipOnError' => true, 'targetClass' => Quiz::className(), 'targetAttribute' => ['quiz_id' => 'quiz_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'quest_id' => 'Questão Id',
            'quiz_id' => 'Quiz Id',
            'subject' => 'Questão',
            'type' => 'Multipla escolha?',
            'created_at' => 'Criado em',
            'updated_at' => 'Última atualização',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'quest_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuiz()
    {
        return $this->hasOne(Quiz::className(), ['quiz_id' => 'quiz_id']);
    }
}
