<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "answer".
 *
 * @property int $answers_id
 * @property string $answer
 * @property int $is_correct
 * @property int $question_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Questions $question
 */
class Answer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['answer', 'question_id', 'created_at', 'updated_at'], 'required'],
            [['answer'], 'string'],
            [['is_correct', 'question_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['question_id' => 'quest_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'answers_id' => 'Resposta ID',
            'answer' => 'Resposta',
            'is_correct' => 'Questão ID',
            'created_at' => 'Criado em',
            'updated_at' => 'Última atualização',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['quest_id' => 'question_id']);
    }
}
